use std::os::fd::AsRawFd;
use std::os::fd::FromRawFd;

use std::fs::File;

use std::os::fd::OwnedFd;

mod proxy;
use anyhow::Result;

use zbus::zvariant::Type;

#[derive(Debug, Type, serde::Deserialize)]
struct Payload {
    time: std::time::SystemTime,
    demo_value: f64,
}

#[async_std::main]
async fn main() -> Result<()> {
    let client_conn = zbus::Connection::session().await?;
    let reply = client_conn
        .call_method(
            Some("se.modio.FdTest"),
            "/se/modio/fdtest",
            Some("se.modio.FdTest"),
            "GetReader",
            &(),
        )
        .await?;

    let fd: zbus::zvariant::Fd = reply.body()?;
    let _fds = reply.take_fds();
    assert!(fd.as_raw_fd() >= 0);

    // Converting an int into a File Descriptor is unsafe, but we do it here.
    // Rather than directly making an Datagram socket from the raw_fd, we make a File, so we can do
    // metadata on it, and then turn it into a socket to work on.
    let f = unsafe { File::from_raw_fd(fd.as_raw_fd()) };
    dbg!(&f);
    let metadata = f.metadata()?;
    dbg!(&metadata);
    let again_fd: OwnedFd = f.into();
    let new_reader: std::os::unix::net::UnixDatagram = again_fd.into();
    let async_reader: async_std::os::unix::net::UnixDatagram = new_reader.into();
    let mut output = vec![0; 1024];
    loop {
        let start = std::time::Instant::now();
        let bytes = async_reader.recv(&mut output).await?;
        let elapsed = start.elapsed().as_millis();
        println!("Read {bytes} of data in {elapsed}ms");
        dbg!(&output[0..bytes]);
        let decoded: Payload = serde_json::from_slice(&output[0..bytes])?;
        let round_elapsed = decoded.time.elapsed()?.as_nanos();
        println!(
            "decoded {decoded:?} round trip: {round_elapsed}ns,  payload {}",
            decoded.demo_value
        );
    }
}
