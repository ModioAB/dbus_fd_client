use zbus::dbus_proxy;

#[dbus_proxy(
    interface = "se.modio.FdTest",
    default_service = "se.modio.FdTest",
    default_path = "/se/modio/fdtest"
)]
/// your bones if you're careless.
trait FdTest {
    /// Get the reader fd
    fn get_reader(&self) -> zbus::Result<zbus::zvariant::Fd>;
}
