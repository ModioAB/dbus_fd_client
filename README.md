# A dbus fd passed client

This is a client (consumer, reader) partner to
<https://gitlab.com/ModioAB/dbus-fd-server> which connects to a running
fd-server on dbus, gets a fd, and turns it into a Unix Datagram Socket to read
data from.


Turning a raw FD into a file descriptor is of course an unsafe operation, as
seen by the unsafe block.


Run under dbus or using:

    dbus-run-session -- cargo run


If the fd-server is not running, there will be an error. 
